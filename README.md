# helm-image-update-policy


## Getting started

A fast and first effort to simplify the work with Image Automation of FluxCD for the developers.
As a developer I don't want to write dozens of k8s objects to enable auto update of images inside of k8s cluster.
Particularly I need to create:

1. secret for accessing the image
2. create ImageRepository
3. create ImagePolicy
4. create ImageUpdateAutomation

Also there is some hidden dependencies between these objects, not expressed by secretRef and other means of referencing different k8s objects.